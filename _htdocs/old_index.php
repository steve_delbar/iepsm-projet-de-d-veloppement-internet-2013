<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!-- Dies ist ein Template von selfhtml.org
 Erstellt wurde es von Jeena Paradies http://jeenaparadies.net/webdesign/leistungen
 Wenn Fragen dazu Auftauchen bitte im Forum posten. -->
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <title>Server2Go</title>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
   <meta http-equiv="Content-Style-Type" content="text/css" />
   <meta name="Content-Language" content="de" />
   <meta name="author" content="Timo Haberkern" /> <!-- Hier sollte der Name des Autors der Inhalte dieser Seite rein. -->
   
    <link href="css/print.css" rel="stylesheet" type="text/css" media="print" /> <!-- siehe screen.css -->
    <link href="css/screen.css" rel="stylesheet" type="text/css" media="screen, projection" /> <!-- Hier sollte der relative Pfad zum Zentralen CSS-File eingetragen werden. Je nachdem in welchem Verzeichniss sich diese Datei befindet muss der Pfad angepasst werden. -->
    <!--[if lte IE 6]><link rel="stylesheet" href="css/ielte6.css" type="text/css" media="screen" /><![endif]--> <!-- �ber Conditional Comments wird f�r den IE <= 6 ein zus�tzlich notwendiges Stylesheet eingebunden -->
 </head>
 <body>
 <div id="container"> 
  <a class="skip" href="#main">Navigation �berspringen</a> 
  <div id="logo">
   <a href="./">Server2Go</a> <!-- Hier kommt noch einmal der Name deiner Homepage rein. Was innerhalb dieses span ist wird invertiert. -->
  </div>
  <ul id="menu">
   <li>Home</li> <!-- Zu der Seite auf der man sich gerade befindet sollte kein Link f�rhen. -->
   <li><a href="documentation.html">Documentation</a></li>
  </ul>
  <div id="main">
   <h1><?php echo $_ENV["S2G_SERVER_SOFTWARE"];?></h1> <!-- Hier sollte die �berschrift f�r die einzelne Seite rein. Auch sehr wichtig f�r Suchmaschinen. -->
    <h2>Congratulation</h2> <!-- Unter�berschriften werden von viele Suchmaschinen auch besonders beachtet. -->
    <p>
       Reading this means that you are running Server2Go successfully. <br>
	   
	   <table style="margin-top: 10px; margin-bottom: 10px; border: 1px solid gray; background-color: #F7FFD4;">
	   		<tr>
	   			<td valign="top">
	   				<img src="img/alert_info.gif" style="border-width: 0px;" border="0">
	   			</td>
	   			<td>
            	   The version you are running supports:<br>
                        <?php
                            $arrParts = explode(" ", $_SERVER["SERVER_SOFTWARE"]);
                        ?>
                       <li><?php echo $arrParts[0]." ".$arrParts[1];?></li>
                   	   <li><?php echo $arrParts[2];?></li>
                   	   <li>SQLite 2</li>
                   	   <?php 
                   	   		 if ( ($_ENV["S2G_EDITION"] == "PSM") || ($_ENV["S2G_EDITION"] == "PSMP") )
                   	   		 {
                                $nConnection = mysql_connect("localhost", "root", "");
                                if ($nConnection)
                                {
                                    echo "<li>";
                                    echo "MySQL ".mysql_get_server_info();
                                    echo "</li>";
                                }
                   	   		 }
                   	   		 
                   	   		 if ($_ENV["S2G_EDITION"] == "PSMP")
                   	   		 {
                   	   		  	echo "<li>Perl 5.8</li>";
                   	   		 }
                   	   ?>
                </td>
            </tr>
        </table>
       If you need other version (i.e. with MySQL) you can get it under <a href="http://www.server2go-web.de">www.server2go-web.de</a>
	</p>
	   
	<h2>Licence</h2>
	<p>
	   Server2Go is Donationware. That means you can download and use it for free and you don't have to pay
	   any royalty charges when distribute an application on CD-ROM that uses Server2Go. But if you use it 
	   commercially or like to keep it free available you should donate to the project. How to donate? Just take
	   a look at the <a href="http://www.server2go-web.de/donation/donation.html">donation</a> page. There are a lot of inexpensive possibilities
	   to help.<br>
	   Every donator who donates 10.- Euro or more is getting a user id and password with that he can download additional software that is not public 
	   available.
	</p>
    <h2>Samples PHP</h2>
    <p>
       <li><a href="phpinfo.php">PHP Informations</a></li>
	   <li><a href="gdlib.php">GD-Lib Test</a></li>
	   <li><a href="sqlitedb.php">SQLite Test</a></li>	   
	   <?php 
	   		 if ( ($_ENV["S2G_EDITION"] == "PSM") || ($_ENV["S2G_EDITION"] == "PSMP") )
	   		 {
	   		  	echo "<li><a href=\"mysql.php\">MySQL Test</a>&nbsp;&nbsp;<a href=\"/phpmyadmin/\">phpMyAdmin</a></li>";
	   		 }
	   		 else
	   		 {
	   		  	 echo "<li>MySQL Test <strong>(MySQL not installed)</strong></li>";
	   		 }
	   ?>
	   
	</p>
	
	<h2>Samples Perl</h2>
    <p>
    
       <?php 
	   		 if ( $_ENV["S2G_EDITION"] == "PSMP") 
	   		 {
	   		  	echo "<li><a href=\"/cgi-bin/printenv.pl\">Environment Variables</a></li>";
	   		 }
	   		 else
	   		 {
	   		  	 echo "<li>Environment Variables <strong>(Perl not installed)</strong></li>";
	   		 }
	   ?>		 
       
	</p>
  </div>
 </div>
 </body>
</html>
