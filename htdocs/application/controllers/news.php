<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {
	
	/**
	 * Index Page for this controller.
	 */
	public function view()
	{
		//$this->output->cache(1);
		
		$data['title'] = 'News - View';
		
		$this->load->database();
		$this->load->model('News_model', 'news');
		$data['news'] = $this->news->liste_news();
		
		$this->load->view('templates/header', $data);
		$this->load->view('news/view', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function form($id = '0')
	{
		//$this->output->cache(1);
		
		if ((int) $id > 0) {
			$data['title'] = 'News - Update';
			$data['form_action'] = '/news/update';
			
			$this->load->database();
			$this->load->model('News_model', 'news');
			$data['news'] = reset($this->news->lire_news($id));
			
		} else {
			$data['title'] = 'News - Add';
			$data['form_action'] = '/news/insert';
			
			$data['news'] = new stdClass();
		}
		
		$this->load->view('templates/header', $data);
		$this->load->view('news/form', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function insert()
	{
		$data['title'] = 'News - Add';
		
		$user = $this->input->post('user');
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		
		$this->load->database();
		$this->load->model('News_model', 'news');
		$data['news'] = $this->news->ajouter_news($user, $title, $content);
		
		$this->load->helper('url');
		redirect('/news/view/', 'refresh');
	}
	
	public function update()
	{
		$id = $this->input->post('id');
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		
		$this->load->database();
		$this->load->model('News_model', 'news');
		$data['news'] = $this->news->editer_news($id, $title, $content);
		
		$this->load->helper('url');
		redirect('/news/view/', 'refresh');
	}
	
	public function delete($id)
	{
		$this->load->database();
		$this->load->model('News_model', 'news');
		$data['news'] = $this->news->supprimer_news((int) $id);
		
		$this->load->helper('url');
		redirect('/news/view/', 'refresh');
	}
	
}

/* End of file pages.php */
/* Location: ./application/controllers/news.php */