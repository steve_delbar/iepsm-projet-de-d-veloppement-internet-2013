<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {

	/**
	 * Index Page for this controller.
	 */
	public function view($page = "index")
	{
		//$this->output->cache(1);
		
		if ( ! file_exists('application/views/pages/'.$page.'.php'))
		{
			if (file_exists('application/views/pages/404.php'))
			{
				$page = "404";
				
			} else {
				// Whoops, we don't have a page for that!
				show_404();
			}
		}
		
		$data['title'] = ucfirst($page); // Capitalize the first letter
		
		if ($page == "news") {
			$this->load->database();
			$this->load->model('News_model', 'news');
			$data['news'] = $this->news->liste_news();
		}
		
		$this->load->view('templates/header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
	
}

/* End of file pages.php */
/* Location: ./application/controllers/pages.php */